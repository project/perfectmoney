<?php

namespace Drupal\perfectmoney\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Pages {

	/**
   * PerfectMoney service.
   *
   * @var \Drupal\perfectmoney\PerfectMoney
   */
  protected $perfectmoney;

  /**
   * Basket service.
   *
   * @var \Drupal\basket\Basket
   */
  protected $basket;

  /**
   * Page Query Params.
   *
   * @var array
   */
  protected $query;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->perfectmoney = \Drupal::service('PerfectMoney');
    $this->basket = \Drupal::hasService('Basket') ? \Drupal::service('Basket') : NULL;
    $this->query = \Drupal::request()->query->all();
  }

	public function title(array $_title_arguments = array(), $_title = ''){
    return $this->perfectmoney->t($_title);
  }

  public function pages($page_type) {
  	$element = [];
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = \Drupal::config('perfectmoney.settings')->get('config');
  	switch($page_type) {
  		case'pay':
        $is_404 = TRUE;
        $payment = $this->perfectmoney->load([
          'id'      => $this->query['pay_id'] ?? '-'
        ]);
        if(!empty($payment) && $payment->status == 'new') {
          $is_404 = FALSE;
          $element['form'] = \Drupal::formBuilder()->getForm('\Drupal\perfectmoney\Form\PaymentForm', $payment);
        }
        if($is_404){
          throw new NotFoundHttpException();
        }
        break;

      case'success':
        $element['#title'] = $this->perfectmoney->t('Successful payment');
        // ---
        $success = !empty($config['success'][$langcode]) ? $config['success'][$langcode] : $config['success']['en'];
        $element += [
          '#theme'       => 'perfectmoney_pages',
          '#info'        => [
            'text'         => [
              '#type'         => 'processed_text',
              '#text'         => $success['value'] ?? '',
              '#format'       => $success['format'] ?? NULL,
            ],
            'type'         => 'success'
          ],
          '#attached'    => [
            'library'      => [
               'perfectmoney/css'
            ]
          ]
        ];
        break;

      case'fail':
        $element['#title'] = $this->perfectmoney->t('Unsuccessful payment');
        // ---
        $fail = !empty($config['fail'][$langcode]) ? $config['fail'][$langcode] : $config['fail']['en'];
        $element += [
          '#theme'       => 'perfectmoney_pages',
          '#info'        => [
            'text'         => [
              '#type'         => 'processed_text',
              '#text'         => $fail['value'] ?? '',
              '#format'       => $fail['format'] ?? NULL,
            ],
            'type'         => 'fail'
          ],
          '#attached'    => [
            'library'      => [
               'perfectmoney/css'
            ]
          ]
        ];
        break;

      case'status':
        if(!empty($config['REMOTE_ADDR'])) {
          $REMOTE_ADDR = explode(PHP_EOL, $config['REMOTE_ADDR']);
          if (!in_array($_SERVER['REMOTE_ADDR'], $REMOTE_ADDR)) {
            throw new NotFoundHttpException();
          }
        }
        if (!$_POST) {
          $_POST = @json_decode(file_get_contents('php://input'), true);
        }
        $hash = strtoupper(md5(implode(':', [
          $_POST['PAYMENT_ID']        ?? '',
          $_POST['PAYEE_ACCOUNT']     ?? '',
          $_POST['PAYMENT_AMOUNT']    ?? '',
          $_POST['PAYMENT_UNITS']     ?? '',
          $_POST['PAYMENT_BATCH_NUM'] ?? '',
          $_POST['PAYER_ACCOUNT']     ?? '',
          strtoupper($config['ALTERNATE_PHRASE_HASH']),
          $_POST['TIMESTAMPGMT']    ?? ''
        ])));
        $V2_HASH = $_POST['V2_HASH'] ?? '';
        if($hash == $V2_HASH) {
          list($orderId, $rand) = explode('-', $_POST['PAYMENT_ID']);
          $payment = $this->perfectmoney->load([
            'id'      => $orderId
          ]);
          if(!empty($payment)) {
            $data = $_POST;
            $prePay = @unserialize($payment->data);
            $data['pre_pay'] = !empty($prePay['pre_pay']) ? $prePay['pre_pay'] : $prePay;
            // ---
            $payment->paytime = time();
            $payment->data = serialize($data);
            $payment->status = 'completed';
            // ---
            $this->perfectmoney->update($payment);
            // ---
            if(!empty($payment->nid) && !empty($this->basket)) {
              if(method_exists($this->basket, 'paymentFinish')) {
                $this->basket->paymentFinish($payment->nid);
              }
            }
            // Alter
            \Drupal::moduleHandler()->alter('perfectmoney_api', $payment, $_POST);
          }
        }
        break;
  	}
  	return $element;
  }
}
