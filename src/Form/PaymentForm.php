<?php

namespace Drupal\perfectmoney\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class PaymentForm extends FormBase {

  /**
   * PerfectMoney service.
   *
   * @var \Drupal\perfectmoney\PerfectMoney
   */
  protected $perfectmoney;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs a new Payeer object.
   */
  public function __construct() {
    $this->perfectmoney = \Drupal::service('PerfectMoney');
    $this->config = \Drupal::config('perfectmoney.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'perfectmoney_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $payment = NULL) {
    $form['#id'] = 'payment_form';
    $form['info'] = [
      '#type'         => 'inline_template',
      '#template'     => '<div class="sum"><label>{{ label }}:</label> {{ sum }} {{ currency }}</div>',
      '#context'      => [
        'label'         => $this->perfectmoney->t('Sum'),
      ],
      '#attached'     => [
        'library'       => [
          'perfectmoney/css'
        ]
      ]
    ];
    // ---
    $payment->result_url    = Url::fromRoute('perfectmoney.pages', ['page_type' => 'success'], ['absolute' => TRUE])->toString();
    $payment->callback_url  = Url::fromRoute('perfectmoney.pages', ['page_type' => 'status'], ['absolute' => TRUE])->toString();
    $payment->cancel_url    = Url::fromRoute('perfectmoney.pages', ['page_type' => 'fail'], ['absolute' => TRUE])->toString();
    // Payment alter
    $this->basketPaymentFormAlter($form, $form_state, $payment);
    // ---
    $form['info']['#context']['currency'] = $form['#params']['currency'];
    $form['info']['#context']['sum'] = $form['#params']['amount'];
    // ---
    $form['actions'] = [
      '#type'         => 'actions',
      'submit'        => [
        '#type'         => 'submit',
        '#value'        => $this->perfectmoney->t('Pay')
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function basketPaymentFormAlter(&$form, &$form_state, $payment) {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = $this->config->get('config');
    $form['#params'] = [
      'amount'          => $payment->amount,
      'currency'        => $payment->currency,
      'order_id'        => $payment->id.'-'.time().rand(0, 999),
      'server_url'      => $payment->callback_url,
      'result_url'      => $payment->result_url,
      'cancel_url'      => $payment->cancel_url,
      'MEMO'            => NULL
    ];
    // Alter
    \Drupal::moduleHandler()->alter('perfectmoney_payment_params', $form['#params'], $payment, $config);
    // ---
    $form['#action'] = 'https://perfectmoney.is/api/step1.asp';
    foreach ([
      'PAYEE_ACCOUNT'               => $config['PAYEE_ACCOUNT']     ?? '',
      'PAYEE_NAME'                  => $config['PAYEE_NAME']        ?? '',
      'PAYMENT_ID'                  => $form['#params']['order_id'],
      'PAYMENT_AMOUNT'              => number_format($form['#params']['amount'], 2, '.', ''),
      'PAYMENT_UNITS'               => $form['#params']['currency'],
      'STATUS_URL'                  => $payment->callback_url,
      'PAYMENT_URL'                 => $payment->result_url,
      'PAYMENT_URL_METHOD'          => 'GET',
      'NOPAYMENT_URL'               => $payment->cancel_url,
      'NOPAYMENT_URL_METHOD'        => 'GET',
      'SUGGESTED_MEMO'              => $form['#params']['MEMO']     ?? ''
    ] as $key => $value) {
      $form[$key] = [
        '#type'       => 'hidden',
        '#value'      => $value,
        '#parents'    => [$key]
      ];
    }
  }

}
