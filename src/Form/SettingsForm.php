<?php

namespace Drupal\perfectmoney\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Url;

/**
 * Class SettingsForm.
 */
class SettingsForm extends FormBase {

  /**
   * PerfectMoney service.
   *
   * @var \Drupal\perfectmoney\PerfectMoney
   */
  protected $perfectmoney;

  /**
   * Ajax info.
   *
   * @var array
   */
  protected $ajax;

  /**
   * Config info.
   *
   * @var object
   */
  protected $config;

  /**
   * Constructs.
   */
  public function __construct() {
    $this->perfectmoney = \Drupal::service('PerfectMoney');
    $this->ajax = [
      'wrapper'       => 'perfectmoney_settings_form_ajax_wrap',
      'callback'      => '::ajaxSubmit'
    ];
    $this->config = \Drupal::config('perfectmoney.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'perfectmoney_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $form += [
      '#prefix'       => '<div id="'.$this->ajax['wrapper'].'">',
      '#suffix'       => '</div>',
      'status_messages'=> [
        '#type'         => 'status_messages'
      ],
      '#title'        => $this->perfectmoney->t('Perfect Money settings')
    ];
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $languages = \Drupal::languageManager()->getLanguages();
    if(count($languages) > 1){
      $options = [];
      foreach ($languages as $language){
        $options[$language->getId()] = $language->getName();
      }
      $form['language'] = [
        '#type'         => 'select',
        '#title'        => t('Language'),
        '#options'      => $options,
        '#ajax'         => $this->ajax,
        '#default_value' => $langcode
      ];
    }
    if(!empty($values['language'])){
      $langcode = $values['language'];
    }
    $form['config'] = [
      '#tree'         => TRUE,
      'PAYEE_ACCOUNT' => [
        '#type'         => 'textfield',
        '#title'        => $this->perfectmoney->t('Wallet number for accepting payments'),
        '#required'     => TRUE,
        '#default_value'=> $this->config->get('config.PAYEE_ACCOUNT'),
      ],
      'PAYEE_NAME'    => [
        '#type'         => 'textfield',
        '#title'        => $this->perfectmoney->t('Name of the recipient of the transfer'),
        '#required'     => TRUE,
        '#default_value'=> $this->config->get('config.PAYEE_NAME'),
      ],
      'ALTERNATE_PHRASE_HASH' => [
        '#type'         => 'textfield',
        '#title'        => $this->perfectmoney->t('MD5 from "Alternative Passphrase" field'),
        '#required'     => TRUE,
        '#default_value'=> $this->config->get('config.ALTERNATE_PHRASE_HASH'),
      ],
      'currency'      => [
        '#type'         => 'select',
        '#title'        => $this->perfectmoney->t('Currency'),
        '#options'      => [
          'USD'           => 'USD',
          'EUR'           => 'EUR',
          'OAU'           => 'OAU',
        ],
        '#default_value' => $this->config->get('config.currency'),
        '#required'     => TRUE
      ],
      'fail'          => [
        '#type'         => 'text_format',
        '#title'        => $this->perfectmoney->t('Unsuccessful payment').' ('.$langcode.')',
        '#default_value'=> $this->config->get('config.fail.'.$langcode.'.value'),
        '#format'       => $this->config->get('config.fail.'.$langcode.'.format'),
        '#parents'      => ['config', 'fail', $langcode]
      ],
      'success'       => [
        '#type'         => 'text_format',
        '#title'        => $this->perfectmoney->t('Successful payment').' ('.$langcode.')',
        '#default_value'=> $this->config->get('config.success.'.$langcode.'.value'),
        '#format'       => $this->config->get('config.success.'.$langcode.'.format'),
        '#parents'      => ['config', 'success', $langcode]
      ],
      'urls'          => [
        '#theme'        => 'table',
        '#rows'         => [
          [
            $this->perfectmoney->t('Success URL'),
            Url::fromRoute('perfectmoney.pages', ['page_type' => 'success'], ['absolute' => TRUE])->toString()
          ],[
            $this->perfectmoney->t('Fail URL'),
            Url::fromRoute('perfectmoney.pages', ['page_type' => 'fail'], ['absolute' => TRUE])->toString()
          ],[
            $this->perfectmoney->t('Status URL'),
            Url::fromRoute('perfectmoney.pages', ['page_type' => 'status'], ['absolute' => TRUE])->toString()
          ]
        ]
      ],
      'REMOTE_ADDR'   => [
        '#type'         => 'textarea',
        '#title'        => '$_SERVER[\'REMOTE_ADDR\']',
        '#default_value'=> $this->config->get('config.REMOTE_ADDR')
      ]
    ];
    $form['actions'] = [
      '#type'         => 'actions',
      'submit'        => [
        '#type'         => 'submit',
        '#name'         => 'save',
        '#value'        => t('Save configuration'),
        '#attributes'   => [
            'class'         => ['button--primary']
        ],
        '#ajax'         => $this->ajax
      ]
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted() && !$form_state->getErrors()) {
      $config = \Drupal::configFactory()->getEditable('perfectmoney.settings');
      $configSave = $form_state->getValue('config');
      if(!empty($preConfig = \Drupal::config('perfectmoney.settings')->get('config'))){
        if(!empty($preConfig['fail'])) {
          $configSave['fail'] += $preConfig['fail'];
        }
        if(!empty($preConfig['success'])) {
          $configSave['success'] += $preConfig['success'];
        }
      }
      $config->set('config', $configSave);
      $config->save();
      \Drupal::messenger()->addMessage(t('The configuration options have been saved.'));
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

}
